class Validator {
  static String confirmPassword = '';
  String? email(String? email){
    RegExp emailRegex = RegExp(r'^[\w\.-]+@[\w-]+\.\w{2,3}(\.\w{2,3})?$');
    final isEmailValid = emailRegex.hasMatch(email ?? '');
    if (!isEmailValid){
      return 'Please enter a valid email';
    }
    return null;
  }
  String? password(String? password){
    confirmPassword = password!;
    if (password.isEmpty) {
      return 'Invalid password';
    } else if (password.length < 6){
      return 'The password must consist of 6 characters';
    }
    return null;
  }
  String? isEmpty(String? text){
    if (text!.isEmpty) {
      return 'Empty field';
    }
    return null;
  }
  String? repeatPassword(String? repeatPassword){
    if (repeatPassword != confirmPassword) {
      return 'The password doesn`t match';
    }
    return null;
  }
  String? numberPhone(String? numberPhone){
    if (numberPhone!.length < 12 || !numberPhone.contains('+')) {
      return 'Invalid number: (count = 12 or >) and (+)';
    }
    return null;
  }
}