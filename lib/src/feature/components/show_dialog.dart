import 'package:flutter/material.dart';
import 'package:manga_app/src/common/style/color_app.dart';

class ShowDialog {
  static void loading(BuildContext context){
    showDialog(
      context: context,
      builder: (context) => const Center(
        child: CircularProgressIndicator(
          color: ColorApp.violet,
        ),
      ),
    );
  }
}