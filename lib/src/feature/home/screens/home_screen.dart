import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:manga_app/src/common/router/router_app.dart';
import 'package:manga_app/src/common/style/color_app.dart';
import 'package:manga_app/src/feature/auth/bloc/auth_repositories_bloc.dart';
import 'package:manga_app/src/feature/components/show_dialog.dart';

class HomeScreen extends StatelessWidget {
   HomeScreen({super.key});

  final User _user = FirebaseAuth.instance.currentUser!;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_user.email.toString()),
      ),
      backgroundColor: ColorApp.white,
      body: Center(
        child: BlocListener<AuthRepositoriesBloc, AuthRepositoriesState>(
          listener: (context, state) {
            if(state is AuthRepositoriesSucces){
              Navigator.pushNamedAndRemoveUntil(context, RouterApp.logIn, (route) => false);
            }
          },
          child: ElevatedButton(
            onPressed: () {
              ShowDialog.loading(context);
              BlocProvider.of<AuthRepositoriesBloc>(context).add(SingOutEvent());
            },
            child: const Text('Exit'),
          ),
        ),
      ),
    );
  }
}
