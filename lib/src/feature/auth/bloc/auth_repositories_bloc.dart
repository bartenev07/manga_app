import 'package:bloc/bloc.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:meta/meta.dart';

import 'package:manga_app/src/common/repositories/auth_repositories/firebase_auth_repository.dart';
import 'package:manga_app/src/feature/components/errorMessage.dart';

part 'auth_repositories_event.dart';
part 'auth_repositories_state.dart';

class AuthRepositoriesBloc extends Bloc<AuthRepositoriesEvent, AuthRepositoriesState> {
  final FirebaseAuthRopository authRepo;
  AuthRepositoriesBloc({required this.authRepo}) : super(AuthRepositoriesInitial()) {
    on<SignUpEvent>((event, emit) async{
        try {
          await authRepo.signUp(event.email, event.password);
          emit(AuthRepositoriesSucces());
        } on FirebaseAuthException catch (e) {
          emit(AuthRepositoriesError());
          ErrorMessage.showSnackBar(e.message);
        }
      }
    );

    on<SignInEvent>(
      (event, emit) async{
        try {
          await authRepo.signIn(event.email, event.password);
          emit(AuthRepositoriesSucces());
        } on FirebaseAuthException catch (e) {
          emit(AuthRepositoriesError());
          ErrorMessage.showSnackBar(e.message);
        }
      }
    );

    on<SingOutEvent>(
      (_, emit) async{
        try {
          await authRepo.logOut();
          await authRepo.singOutGoogle();
          emit(AuthRepositoriesSucces());
        } on FirebaseAuthException catch (e) {
          emit(AuthRepositoriesError());
          ErrorMessage.showSnackBar(e.message);
        }
      }
    );

    on<SingGoogleEvent>(
      (_, emit) async{
        try {
          final GoogleSignInAccount? signInAccount = await GoogleSignIn().signIn();
          if (signInAccount != null) {
            await authRepo.singGoogle(signInAccount);
            emit(GoogleSingInSucces());
          }else{
            emit(AuthRepositoriesError());
          }
        } on FirebaseAuthException catch (e) {
          emit(AuthRepositoriesError());
          ErrorMessage.showSnackBar(e.message);
        }
      }
    );
}
}