part of 'auth_repositories_bloc.dart';

@immutable
abstract class AuthRepositoriesState {}

final class AuthRepositoriesInitial extends AuthRepositoriesState {}

final class AuthRepositoriesSucces extends AuthRepositoriesState {}

final class GoogleSingInSucces extends AuthRepositoriesState {}

final class AuthRepositoriesError extends AuthRepositoriesState {}

