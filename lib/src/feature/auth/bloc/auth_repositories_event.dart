part of 'auth_repositories_bloc.dart';

@immutable
abstract class AuthRepositoriesEvent {}

class SignUpEvent extends AuthRepositoriesEvent {
  final String email;
  final String password;

  SignUpEvent({required this.email, required this.password});
}

class SignInEvent extends AuthRepositoriesEvent {
  final String email;
  final String password;

  SignInEvent({required this.email, required this.password});
}

class SingOutEvent extends AuthRepositoriesEvent{}

class SingGoogleEvent extends AuthRepositoriesEvent{}





