import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:manga_app/src/common/style/color_app.dart';
import 'package:manga_app/src/common/style/text_app.dart';

class MyTextField extends StatelessWidget {
  final TextEditingController controller;
  final String? Function(String? value)? validator;
  final String textHint;
  final Widget? prefixIcon;
  final List<TextInputFormatter>? inputFormatters;
  final TextInputType? keyboardType;
  const MyTextField(
    {super.key,
      required this.controller,
      this.validator,
      required this.textHint,
      this.prefixIcon, 
      this.inputFormatters, 
      this.keyboardType
    });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      inputFormatters: inputFormatters,
      keyboardType: keyboardType,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.symmetric(horizontal: 15),
        hintText: textHint,
        prefixIcon: prefixIcon,
        border: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(20)),
        ),
        filled: true,
        fillColor: ColorApp.white.withOpacity(0.8),
        hintStyle: TextStyleApp.textField,
      ),
      validator: validator,
    );
  }
}

class MyTextFieldPassw extends StatefulWidget {
  final TextEditingController? passwordController;
  final String hintText;
  final String? Function(String? value)? validator;
  const MyTextFieldPassw(
      {super.key,
      this.passwordController,
      required this.hintText,
      this.validator});
  @override
  State<MyTextFieldPassw> createState() => _MyTextFieldPasswState();
}

class _MyTextFieldPasswState extends State<MyTextFieldPassw> {
  bool _isObscureText = true;
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget.passwordController,
      obscureText: _isObscureText,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.symmetric(horizontal: 15),
        hintText: widget.hintText,
        hintStyle: TextStyleApp.textField,
        prefixIcon: const Icon(
          Icons.lock,
          color: ColorApp.blue,
        ),
        suffixIcon: IconButton(
          padding: const EdgeInsets.only(right: 10),
          onPressed: () {
            setState(() {
              _isObscureText = !_isObscureText;
            });
          },
          icon: _isObscureText
              ? const Icon(Icons.visibility_off)
              : const Icon(Icons.visibility),
          color: ColorApp.blue,
        ),
        border: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(20)),
        ),
        filled: true,
        fillColor: ColorApp.white.withOpacity(0.8),
      ),
      validator: widget.validator,
    );
  }
}
