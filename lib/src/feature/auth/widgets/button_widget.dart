import 'package:flutter/material.dart';
import 'package:manga_app/src/common/style/color_app.dart';
import 'package:manga_app/src/common/style/text_app.dart';

class MyButtonWidget extends StatelessWidget {
  final void Function()? onTap;
  final String text;
  const MyButtonWidget({super.key, this.onTap, required this.text});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: double.infinity,
        height: 46,
        decoration:  BoxDecoration(
          border: Border.all(color: ColorApp.white),
          borderRadius: BorderRadius.circular(20)
        ),
        child: Center(
          child: Text(
            text,
            style: TextStyleApp.bodyOne,
          ),
        ),
      ),
    );
  }
}