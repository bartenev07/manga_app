import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:manga_app/src/common/style/color_app.dart';

class WebButtonWidget extends StatelessWidget {
  final String assetSvg;
  final void Function()? onTap;
  const WebButtonWidget({super.key, required this.assetSvg, this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.all(12),
        decoration: BoxDecoration(
          border: Border.all(color: ColorApp.violet, ),
          borderRadius: BorderRadius.circular(50)
        ),
        child: SvgPicture.asset(assetSvg, width: 30, height: 30),
      ),
    );
  }
}

