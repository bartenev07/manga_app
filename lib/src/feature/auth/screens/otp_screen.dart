// ignore_for_file: unused_field
import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:manga_app/src/common/repositories/auth_repositories/firebase_auth_repository.dart';
import 'package:manga_app/src/common/style/color_app.dart';
import 'package:manga_app/src/feature/components/view/component_view.dart';
import 'package:pinput/pinput.dart';

class OtpScreen extends StatefulWidget {
  final String namePathNavigator;
  final String numberPhone;
  final String verificationId;

  const OtpScreen({super.key, required this.namePathNavigator, required this.numberPhone, required this.verificationId});

  @override
  State<OtpScreen> createState() => _OtpScreenState();
}

class _OtpScreenState extends State<OtpScreen> {

  bool _isResendAgain = false;
  final _formKey = GlobalKey<FormState>();
  final FirebaseAuthRopository _authRepo = FirebaseAuthRopository();
  late Timer _timer;
  int _start = 60;

  void resend() {
    setState(() {
      _isResendAgain = true;
    });
    const oneSec = Duration(seconds: 1);
    _timer = Timer.periodic(oneSec, (timer) {
      setState(() {
        if (_start == 0) {
          _start = 60;
          _isResendAgain = false;
          timer.cancel();
        } else {
          _start--;
        }
      });
    });
  }

  void otp(String code) async{
    try {
      await _authRepo.otpCredential(widget.verificationId, code);
      Navigator.pushNamedAndRemoveUntil(context, widget.namePathNavigator, (route) => false);                   
    } on FirebaseAuthException catch (e) {
      Navigator.pop(context);
      ErrorMessage.showSnackBar(e.message);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorApp.backgroundApp,
        foregroundColor: ColorApp.white,
      ),
      body: LayoutBuilder(
        builder: (context, constraints) => SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 20),
            child: ConstrainedBox(
              constraints: BoxConstraints(minHeight: constraints.maxHeight),
              child: IntrinsicHeight(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'Enter code',
                      style: TextStyle(color: ColorApp.white, fontSize: 25),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Text(
                      'Code sent to you mobile number\n${widget.numberPhone}',
                      style: const TextStyle(color: ColorApp.white, fontSize: 15),
                    ),
                    const SizedBox(
                      height: 40,
                    ),
                    Form(
                      key: _formKey,
                      child: Pinput(
                        length: 6,
                        showCursor: false,
                        inputFormatters: [
                          FilteringTextInputFormatter.digitsOnly
                        ],
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        defaultPinTheme: PinTheme(
                          width: 50,
                          height: 50,
                          textStyle: const TextStyle(fontSize: 24, color: ColorApp.blue),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: ColorApp.white.withOpacity(0.8),
                            border: Border.all(
                              color: ColorApp.violet,
                              width: 2,
                            ),
                          ),
                        ),
                        validator: (value) {
                          ShowDialog.loading(context);
                          otp(value!);
                          return null;
                        },
                      ),
                    ),
                    const SizedBox(
                      height: 60,
                    ),
                    Center(
                      child: TextButton(
                        onPressed: () async {
                          if (_isResendAgain) return;
                          ShowDialog.loading(context);
                          try {
                            await _authRepo.verificPhone(
                              phoneNumber: widget.numberPhone, 
                              verificationCompleted:(p0) {}, 
                              verificationFailed:(FirebaseAuthException e) {
                                Navigator.pop(context);
                                ErrorMessage.showSnackBar(e.message);
                              }, 
                              codeSent:(verId, p1) {
                                Navigator.pop(context);
                              }, 
                              codeAutoRetrievalTimeout:(p0) {},
                            );
                          } on FirebaseAuthException catch (e) {
                            Navigator.pop(context);
                            ErrorMessage.showSnackBar(e.message);
                          }
                          setState(() {
                            if (_isResendAgain) return;
                            resend();
                          });
                        },
                        child: Text(
                            _isResendAgain
                                ? 'Resend code in $_start'
                                : 'Resend code',
                            style: const TextStyle(
                                color: ColorApp.violet, fontSize: 17)),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
