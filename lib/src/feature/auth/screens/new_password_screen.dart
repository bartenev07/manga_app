import 'package:flutter/material.dart';
import 'package:manga_app/src/common/router/router_app.dart';
import 'package:manga_app/src/feature/auth/view/auth_widget_view.dart';
import 'package:manga_app/src/feature/components/validator.dart';
import 'package:manga_app/src/common/style/color_app.dart';

class NewPasswordScreen extends StatelessWidget {
  NewPasswordScreen({super.key});
  
  final TextEditingController _controllerPass = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LayoutBuilder(
        builder: (context, constraints) => SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(minHeight: constraints.maxHeight),
            child: IntrinsicHeight(
              child: Padding(
                padding: const EdgeInsets.only(right: 18, left: 18,bottom: 40, top: 50),
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        'Create new password',
                        style: TextStyle(color: ColorApp.white, fontSize: 25),
                      ),
                      const SizedBox(height: 15),
                      const Text(
                        'Please enter and confirm your new password',
                        style: TextStyle(color: Color.fromARGB(255, 225, 225, 225), fontSize: 15),
                      ),
                      const SizedBox(height: 25),
                      const Text(
                        'New password',
                        style: TextStyle(color: ColorApp.white, fontSize: 20),
                      ),
                      const SizedBox(height: 8),
                      MyTextFieldPassw(
                        passwordController: _controllerPass, 
                        hintText: 'Enter new password',
                        validator: (value) => Validator().password(value),
                      ),
                      const SizedBox(height: 12),
                      const Text(
                        'Confirm new password',
                        style: TextStyle(color: ColorApp.white, fontSize: 20),
                      ),
                      const SizedBox(height: 8),
                      MyTextFieldPassw(
                        hintText: 'Enter confirm password',
                        validator: (value) => Validator().repeatPassword(value),
                      ),
                      const SizedBox(height: 20),
                      const Spacer(),
                      MyButtonWidget(
                        text: 'Next', 
                        onTap: () => _formKey.currentState!.validate() ? Navigator.of(context).pushNamed(RouterApp.logIn) : null,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}