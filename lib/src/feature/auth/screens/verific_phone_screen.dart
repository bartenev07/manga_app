// ignore_for_file: unnecessary_null_comparison
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:manga_app/src/common/repositories/auth_repositories/firebase_auth_repository.dart';
import 'package:manga_app/src/feature/auth/screens/otp_screen.dart';
import 'package:manga_app/src/feature/auth/view/auth_widget_view.dart';
import 'package:manga_app/src/common/style/color_app.dart';
import '../../components/view/component_view.dart';

class VerificPhoneScreen extends StatefulWidget {
  final String router;
  final String nameScreen;

  const VerificPhoneScreen({super.key, required this.router, required this.nameScreen});

  @override
  State<VerificPhoneScreen> createState() => _VerificPhoneScreenState();
}

class _VerificPhoneScreenState extends State<VerificPhoneScreen> {

  final _formKey = GlobalKey<FormState>();
  final TextEditingController _phoneNumberController = TextEditingController();
  final FirebaseAuthRopository _authRepo = FirebaseAuthRopository();

  @override
  void dispose() {
    _phoneNumberController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorApp.backgroundApp,
        foregroundColor: ColorApp.white,
      ),
      body: LayoutBuilder(
      builder: (context, constraints) => SingleChildScrollView(
        child: ConstrainedBox(
          constraints: BoxConstraints(minHeight: constraints.maxHeight),
          child: IntrinsicHeight(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.nameScreen,
                    style: const TextStyle(color: ColorApp.white, fontSize: 25),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  const Text(
                    'We will send you a text with a verification code',
                    style: TextStyle(
                        color: Color.fromARGB(255, 225, 225, 225),
                        fontSize: 15),
                  ),
                  const SizedBox(height: 25),
                  Form(
                    key: _formKey,
                    child: MyTextField(
                      controller: _phoneNumberController,
                      textHint: '+1 000 000 0000',
                      inputFormatters: [FilteringTextInputFormatter.allow(RegExp('[0-9+]+'))],
                      keyboardType: TextInputType.phone,
                      prefixIcon: const Icon(
                        Icons.phone,
                        color: ColorApp.blue,
                        size: 20,
                      ),
                      validator: (value) => Validator().numberPhone(value),
                    ),
                  ),
                  const Spacer(),
                  MyButtonWidget(
                    text: 'Next',
                    onTap: () async {
                      if (_formKey.currentState!.validate()) {
                        ShowDialog.loading(context);
                        try {
                          await _authRepo.verificPhone(
                            phoneNumber: _phoneNumberController.text, 
                            verificationCompleted:(p0) {}, 
                            verificationFailed:(FirebaseAuthException e) {
                              Navigator.pop(context);
                              ErrorMessage.showSnackBar(e.message);
                            }, 
                            codeSent:(verId, p1) {
                              Navigator.push(
                                context, 
                                MaterialPageRoute(builder:(context) => OtpScreen(
                                    namePathNavigator: widget.router, 
                                    numberPhone: _phoneNumberController.text, 
                                    verificationId: verId,
                                  ),
                                )
                              );
                            }, 
                            codeAutoRetrievalTimeout:(p0) {},
                          );
                        } on FirebaseAuthException catch (e) {
                          Navigator.pop(context);
                          ErrorMessage.showSnackBar(e.message);
                        }
                      }
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    )
    );
  }
}
