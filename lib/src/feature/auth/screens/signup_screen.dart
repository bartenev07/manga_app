import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:manga_app/src/common/router/router_app.dart';
import 'package:manga_app/src/feature/auth/bloc/auth_repositories_bloc.dart';
import 'package:manga_app/src/common/style/color_app.dart';
import 'package:manga_app/src/common/style/text_app.dart';
import 'package:manga_app/src/feature/auth/screens/verific_phone_screen.dart';
import '../../../common/repositories/shared_preference_repo/shared_preference.dart';
import '../../components/view/component_view.dart';
import '../view/auth_widget_view.dart';

class SingUpScreen extends StatelessWidget {
  SingUpScreen({super.key});

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _userNameController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LayoutBuilder(
        builder: (context, constraints) => SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(minHeight: constraints.maxHeight),
            child: IntrinsicHeight(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      const Spacer(),
                      const Text(
                        'MANGA',
                        style: TextStyleApp.headerTwo,
                      ),
                      const Text(
                        'FLIX',
                        style: TextStyleApp.headerOne,
                      ),
                      const Spacer(),
                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Sign up',
                          style: TextStyleApp.headerThree,
                        ),
                      ),
                      const SizedBox(height: 10),
                      MyTextField(
                        controller: _emailController,
                        textHint: 'Enter your email',
                        prefixIcon: const Icon(
                          Icons.email,
                          color: ColorApp.blue,
                        ),
                        validator: (value) => Validator().email(value),
                      ),
                      const SizedBox(height: 10),
                      MyTextField(
                        controller: _userNameController,
                        textHint: 'Enter your username',
                        prefixIcon: const Icon(
                          Icons.person,
                          color: ColorApp.blue,
                        ),
                        validator: (value) => Validator().isEmpty(value),
                      ),
                      const SizedBox(height: 10),
                      MyTextFieldPassw(
                        passwordController: _passwordController,
                        hintText: 'Enter your password',
                        validator: (value) => Validator().password(value),
                      ),
                      const SizedBox(height: 10),
                      MyTextFieldPassw(
                        hintText: 'Enter your password',
                        validator: (value) => Validator().repeatPassword(value),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 15),
                        child: GestureDetector(
                          onTap: () => Navigator.pushReplacementNamed(context, RouterApp.logIn),
                          child: const Text('There is an account?',
                            style: TextStyle(
                              color: ColorApp.violet, 
                              fontSize: 16,
                            )
                          ),
                        ),
                      ),
                      const Spacer(),
                      BlocListener<AuthRepositoriesBloc, AuthRepositoriesState>(
                        listener: (context, state) {
                          if (state is AuthRepositoriesSucces) {
                            Navigator.push(context, MaterialPageRoute(
                              builder:(context) => const VerificPhoneScreen(
                                router: RouterApp.home,
                                nameScreen: 'Phone verification',
                              ),
                              )
                            ).then((_) {
                              FirebaseAuth.instance.currentUser?.delete();
                              Navigator.pushReplacementNamed(context, RouterApp.signUp);
                            });
                          } else if (state is GoogleSingInSucces) {
                            SharedPreferenceRepo().setBool('googlesignin', true);//Записать что была googl регистр.
                            Navigator.pushNamedAndRemoveUntil(context, RouterApp.home, (route) => false);
                          } else if(state is AuthRepositoriesError){
                            Navigator.pushNamedAndRemoveUntil(context, RouterApp.signUp, (route) => false);
                          }
                        },
                        child: MyButtonWidget(
                          text: 'Registr',
                          onTap: () {
                            if (_formKey.currentState!.validate()) {
                              ShowDialog.loading(context);
                              SharedPreferenceRepo().setBool('remember', true);//Записать что была обычная регистр.
                              BlocProvider.of<AuthRepositoriesBloc>(context).add(SignUpEvent(
                                email: _emailController.text,
                                password: _passwordController.text));
                            }
                          },
                        ),
                      ),
                      const SizedBox(height: 15),
                      const Text(
                        'OR',
                        style: TextStyleApp.bodeTwo,
                      ),
                      const SizedBox(height: 15),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          WebButtonWidget(assetSvg: 'assets/svg/icons-yandex.svg'),
                          WebButtonWidget(assetSvg: 'assets/svg/vk.svg'),
                          WebButtonWidget(
                            assetSvg: 'assets/svg/icons-google.svg',
                            onTap: () {
                              ShowDialog.loading(context);
                              BlocProvider.of<AuthRepositoriesBloc>(context).add(SingGoogleEvent());
                            },
                          ),
                        ],
                      ),
                      const Spacer(),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
