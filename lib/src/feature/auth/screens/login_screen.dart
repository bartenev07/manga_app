import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:manga_app/src/common/router/router_app.dart';
import 'package:manga_app/src/feature/auth/bloc/auth_repositories_bloc.dart';
import 'package:manga_app/src/feature/auth/screens/verific_phone_screen.dart';
import 'package:manga_app/src/feature/auth/view/auth_widget_view.dart';
import 'package:manga_app/src/common/style/color_app.dart';
import 'package:manga_app/src/common/style/text_app.dart';
import '../../../common/repositories/shared_preference_repo/shared_preference.dart';
import '../../components/view/component_view.dart';

class LogInScreen extends StatefulWidget {
  const LogInScreen({super.key});
  @override
  State<LogInScreen> createState() => _LogInScreenState();
}

class _LogInScreenState extends State<LogInScreen> {

  bool _isRemember = false;
  final _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: LayoutBuilder(
      builder: (context, constraints) => SingleChildScrollView(
        child: ConstrainedBox(
          constraints: BoxConstraints(minHeight: constraints.maxHeight),
          child: IntrinsicHeight(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 14),
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    const Spacer(),
                    const Text(
                      'MANGA',
                      style: TextStyleApp.headerTwo,
                    ),
                    const Text(
                      'FLIX',
                      style: TextStyleApp.headerOne,
                    ),
                    const Spacer(),
                    const Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Login',
                        style: TextStyleApp.headerThree,
                      ),
                    ),
                    const SizedBox(height: 10),
                    MyTextField(
                      controller: _emailController,
                      textHint: 'Enter your email',
                      validator: (value) => Validator().email(value),
                      prefixIcon: const Icon(
                        Icons.email,
                        color: ColorApp.blue,
                      ),
                    ),
                    const SizedBox(height: 15),
                    MyTextFieldPassw(
                      passwordController: _passwordController,
                      hintText: 'Enter your password',
                      validator: (value) => Validator().password(value),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        TextButton.icon(
                          onPressed: () {
                            setState(() {
                              _isRemember = !_isRemember;
                            });
                          },
                          style: TextButton.styleFrom(
                            foregroundColor: ColorApp.violet,
                          ),
                          label: const Text('Remember me'),
                          icon: _isRemember
                              ? const Icon(
                                  Icons.check_box_outlined,
                                  size: 17,
                                )
                              : const Icon(
                                  Icons.check_box_outline_blank,
                                  size: 17,
                                ),
                        ),
                        TextButton(
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(
                              builder: (context) => const VerificPhoneScreen(
                                router: RouterApp.newPassword,
                                nameScreen: 'Forgot your password?',
                              ),
                            ));
                          },
                          style: TextButton.styleFrom(
                            foregroundColor: ColorApp.violet,
                          ),
                          child: const Text('Forgot Password?'),
                        ),
                      ],
                    ),
                    const Spacer(),
                    BlocListener<AuthRepositoriesBloc, AuthRepositoriesState>(
                      listener: (context, state) {
                        if (state is AuthRepositoriesSucces) {
                          Navigator.pushNamedAndRemoveUntil(context, RouterApp.home, (route) => false);
                        } else if (state is GoogleSingInSucces) {
                          SharedPreferenceRepo().setBool('googlesignin', true);//Записать, что зашел через google 
                          Navigator.pushNamedAndRemoveUntil(context, RouterApp.home, (route) => false);
                        } else if(state is AuthRepositoriesError){
                          Navigator.pushNamedAndRemoveUntil(context, RouterApp.logIn, (route) => false);
                        }
                      },
                      child: MyButtonWidget( // обычный вход
                          text: 'login',
                          onTap: () {
                            if (_formKey.currentState!.validate()) {
                              ShowDialog.loading(context);
                              SharedPreferenceRepo().setBool('remember', _isRemember);//Запитсать стоит ли флажок на RememberMe
                              SharedPreferenceRepo().setBool('googlesignin', false);//Записать, что это не Google вход 
                              BlocProvider.of<AuthRepositoriesBloc>(context).add(SignInEvent(
                                email: _emailController.text,
                                password: _passwordController.text,
                              ));
                            }
                          } //=> _formKey.currentState!.validate() ? signIn() : null,
                      ),
                    ),
                    const SizedBox(height: 20),
                    const Text(
                      'OR',
                      style: TextStyleApp.bodeTwo,
                    ),
                    const SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        WebButtonWidget(
                            assetSvg: 'assets/svg/icons-yandex.svg'),
                        WebButtonWidget(assetSvg: 'assets/svg/vk.svg'),
                        WebButtonWidget(
                          assetSvg: 'assets/svg/icons-google.svg',
                          onTap: () {
                            ShowDialog.loading(context);
                            BlocProvider.of<AuthRepositoriesBloc>(context).add(SingGoogleEvent());//Googl вход
                          },
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text(
                          'No account?',
                          style: TextStyleApp.bodeTwo,
                        ),
                        const SizedBox(width: 5),
                        GestureDetector(
                          onTap: () => Navigator.pushReplacementNamed(context, RouterApp.signUp),
                          child: const Text(
                            'Sign Up',
                            style: TextStyle(
                              fontSize: 16,
                              color: ColorApp.violet,
                            ),
                          ),
                        ),
                      ],
                    ),
                    const Spacer(),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    ));
  }
}
