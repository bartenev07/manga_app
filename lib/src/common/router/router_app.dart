import 'package:manga_app/src/common/config/auth_gate.dart';
import 'package:manga_app/src/feature/home/screens/home_screen.dart';
import '../../feature/auth/view/auth_screens_view.dart';

class RouterApp {
  static final router = {
      authGate:(context) => AuthGate(),
      logIn: (context) => const LogInScreen(),
      signUp: (context) => SingUpScreen(),
      newPassword: (context) => NewPasswordScreen(),
      home:(context) => HomeScreen(),
  };
  static const start = '/';
  static const authGate = '/authgate';
  static const logIn = '/logIn';
  static const signUp = '/signUp';
  static const newPassword = '/newPassword';
  static const home = '/home';
}