import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:manga_app/src/common/repositories/shared_preference_repo/shared_preference.dart';
import 'package:manga_app/src/feature/auth/screens/login_screen.dart';
import 'package:manga_app/src/feature/home/screens/home_screen.dart';

// класс, для автоматический вход, без повторного ввода данных
class AuthGate extends StatelessWidget {
  AuthGate({super.key});
  
  final User? _user = FirebaseAuth.instance.currentUser;

  Widget _onSystem(){ //проверка зашел ли пользватель в систему
    if (_user != null) {
      return  HomeScreen();
    }else{
      return const LogInScreen();
    }
  }

  @override
  Widget build(BuildContext context)  {
    return FutureBuilder(
      future: Future.wait([
        SharedPreferenceRepo().getBool('remember'),// Получить флажок rememberMe, если пользователь поставил, то приследующем входе зайдет в приложение без входа
        SharedPreferenceRepo().getBool('googlesignin')//Получить зашел ли через google, если да то вход осущев. без зависимости нажат флажек rememberMe или нет 
      ]), 
      builder:(context, AsyncSnapshot<List<bool>> snapshot) {
        if (snapshot.data?[1] == true) {
          return _onSystem();
        }else{
          if (snapshot.data?[0] == false) {
            FirebaseAuth.instance.signOut();
            return const LogInScreen();
          }else{
            return _onSystem();
          }
        }    
      },
    );
  }
}