import 'package:flutter/material.dart';
import 'package:manga_app/src/common/style/color_app.dart';

class DataTheme {
  static final dataTheme = ThemeData(
    scaffoldBackgroundColor: ColorApp.backgroundApp,
    fontFamily: 'NotoSans'
  );
}