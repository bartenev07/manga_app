import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class FirebaseAuthRopository {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  //SingIn
  Future<void> signIn(String emailController, passwordController) async {
    await _auth.signInWithEmailAndPassword(
      email: emailController, 
      password: passwordController
    );
  }
  //SingUp
  Future<void> signUp(String emailController, passwordController) async {
    await _auth.createUserWithEmailAndPassword(
      email: emailController, 
      password: passwordController
    );
  }
  //SingOut
  Future<void> logOut() async {
    await _auth.signOut();
  }
  //Otp
  Future otpCredential(String verifi, code) async {
    final credential = PhoneAuthProvider.credential(
      verificationId: verifi, 
      smsCode: code
    );
    return _auth.signInWithCredential(credential);
  }
  //VerificatoinPhone
  Future<void> verificPhone({
      required String phoneNumber,
      required void Function(PhoneAuthCredential) verificationCompleted,
      required void Function(FirebaseAuthException) verificationFailed,
      required void Function(String, int?) codeSent,
      required void Function(String) codeAutoRetrievalTimeout,
    }) async {
    await _auth.verifyPhoneNumber(
      verificationCompleted: verificationCompleted, 
      verificationFailed: verificationFailed, 
      codeSent: codeSent,  
      codeAutoRetrievalTimeout: codeAutoRetrievalTimeout, 
      phoneNumber: phoneNumber,
    );
  }
  //SingInGoogle
  Future singGoogle(GoogleSignInAccount? signInAccount) async{
    final GoogleSignInAuthentication singInAuth = await signInAccount!.authentication;
    final credential = GoogleAuthProvider.credential(
      accessToken: singInAuth.accessToken,
      idToken: singInAuth.idToken,
    );
    return await _auth.signInWithCredential(credential);
  }
  //SingOut
  Future<void> singOutGoogle() async {
    await GoogleSignIn().signOut();
  }
}