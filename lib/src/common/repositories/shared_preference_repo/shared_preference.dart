import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferenceRepo {
  //Записать на хранилище телефона bool
  Future setBool(String key, bool value) async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool(key, value);
  }
  //Взять из хранилища телефона bool, если результат null вернет false
  Future<bool> getBool(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(key) ?? false;
  }
}