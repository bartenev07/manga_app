import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:manga_app/src/common/repositories/auth_repositories/firebase_auth_repository.dart';
import 'package:manga_app/src/common/router/router_app.dart';
import 'package:manga_app/src/common/style/theme_data.dart';
import 'package:manga_app/src/feature/auth/bloc/auth_repositories_bloc.dart';
import 'package:manga_app/src/feature/components/errorMessage.dart';
import 'package:manga_app/firebase_options.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({
    super.key,
  });
  @override
  Widget build(BuildContext context) {
    return RepositoryProvider(
      create: (context) => FirebaseAuthRopository(),
      child: BlocProvider(
        create: (context) => AuthRepositoriesBloc(authRepo: RepositoryProvider.of<FirebaseAuthRopository>(context)
        ),
        child: MaterialApp(
          scaffoldMessengerKey: ErrorMessage.messangerKey,
          theme: DataTheme.dataTheme,
          initialRoute: RouterApp.authGate,
          routes: RouterApp.router,
          debugShowCheckedModeBanner: false,
        ),
      ),
    );
  }
}
